package com.example.encryption

import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.JsonReader
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKeys
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.recyclerviewwithgroupdate.adapter.MyAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import javax.microedition.khronos.opengles.GL10

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val preferencesName = "SharedPreferences"
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var arrayPersons: ArrayList<Person>
    private lateinit var adapter: MyAdapter

    var person: MutableList<Person> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setup()

    }


    override fun onClick(view: View) {
        when (view.id) {

            R.id.butSave -> {
                if (tviName.text.toString().isEmpty() ||
                    tviAge.text.toString().isEmpty() ||
                    tviGmail.text.toString().isEmpty()
                ) {
                    Toast.makeText(applicationContext, "Ingrese dato", Toast.LENGTH_SHORT).show()
                } else {
                    saveStorage(savePerson())
                    showJson()
                    loadRecycler()
                }

            }

            R.id.butEdit -> {
                if (tviName.text.toString().isEmpty() ||
                    tviAge.text.toString().isEmpty() ||
                    tviGmail.text.toString().isEmpty() ||
                    tviIndex.text.toString().isEmpty()
                ) {
                    Toast.makeText(
                        applicationContext,
                        "Seleccione Persona a Editar",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(applicationContext, "Editando...", Toast.LENGTH_SHORT).show()
                    editPerson(
                        tviIndex.text.toString().toInt(),
                        Person(
                            tviName.text.toString(),
                            tviGmail.text.toString(),
                            tviAge.text.toString().toInt()
                        )
                    )
                    loadRecycler()
                    showJson()
                }
            }

            R.id.butDelete -> {
                if (tviName.text.toString().isEmpty() ||
                    tviAge.text.toString().isEmpty() ||
                    tviGmail.text.toString().isEmpty() ||
                    tviIndex.text.toString().isEmpty()
                ) {
                    Toast.makeText(
                        applicationContext,
                        "Seleccione Persona a eliminar",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(applicationContext, "elimando...", Toast.LENGTH_SHORT).show()
                    deletePerson(tviIndex.text.toString().toInt())
                    loadRecycler()
                    showJson()
                }
            }
        }
    }


    private fun setup() {
        this.butSave.setOnClickListener(this)
        this.butDelete.setOnClickListener(this)
        this.butEdit.setOnClickListener(this)
        initEncryptedSharedPreference()
        showJson()
        loadRecycler()

       // butEdit.isClickable=false
       // butDelete.isClickable=false



    }


    private fun initEncryptedSharedPreference() {
        val keyGenParameterSpec = KeyGenParameterSpec.Builder(
            MasterKey.DEFAULT_MASTER_KEY_ALIAS,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        ).setBlockModes(KeyProperties.BLOCK_MODE_GCM)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
            .setKeySize(MasterKey.DEFAULT_AES_GCM_MASTER_KEY_SIZE)
            .build()

        val masterKeyAlias =
            MasterKey.Builder(applicationContext, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                .setKeyGenParameterSpec(keyGenParameterSpec)
                .build()

        sharedPreferences = EncryptedSharedPreferences.create(
            applicationContext,
            preferencesName,
            masterKeyAlias,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

/*    private fun showSourceFile() {
        val preferencesFile = File("${applicationInfo.dataDir}/shared_prefs/$preferencesName.xml")
        if (preferencesFile.exists()) {
            tviRawfile.text = preferencesFile.readText()
        } else {
            tviRawfile.text = ""
        }
    }
    */

    fun showJson() {
        val value = sharedPreferences.getString("DATA", "")
        var gson = Gson()  /*var person = gson.fromJson(value,Person::class.java)*/
        tviRead.text = value

    }

    fun savePerson(): MutableList<Person> {
        person = readStorage().toCollection(ArrayList())
        person.add(
            Person(
                tviName.text.toString(),
                tviGmail.text.toString(),
                tviAge.text.toString().toInt()
            )
        )
        clearEditTexts()
        return person
    }

    fun editPerson(index: Int, per: Person): MutableList<Person> {
        person = readStorage().toCollection(ArrayList())
        person.set(index, per)
        saveStorage(person)
        clearEditTexts()
        return person

    }

    fun deletePerson(index: Int): MutableList<Person> {
        person = readStorage().toCollection(ArrayList())
        person.removeAt(index)
        saveStorage(person)
        clearEditTexts()
        return person
    }

    fun saveStorage(person: MutableList<Person>) {
        var gson = Gson()
        var jsonString = gson.toJson(person)
        //save
        sharedPreferences.edit()
            .putString("DATA", jsonString)
            .apply()
    }


    fun readStorage(): Array<Person> {
        val value = sharedPreferences.getString("DATA", "")
        var gson = Gson()
        val arrayPerson = object : TypeToken<Array<Person>>() {}.type
        var data: Array<Person> = gson.fromJson(value, arrayPerson)
        if (value != null) {
            if (value.isEmpty()) {
                println("datos vacios")
            } else {
                return data
            }
        }
        return data
    }


    fun loadRecycler() {
        arrayPersons = ArrayList()
        arrayPersons = readStorage().toCollection(ArrayList())
        val mLayoutManager = LinearLayoutManager(applicationContext)
        rviPersons.setLayoutManager(mLayoutManager)
        adapter = MyAdapter(this, arrayPersons)
        rviPersons.setAdapter(adapter)
    }

    fun clearEditTexts() {
        tviName.setText("")
        tviAge.setText("")
        tviGmail.setText("")
        tviIndex.setText("")

        //butEdit.isClickable=false
        //butDelete.isClickable=false
    }


}