package com.test.recyclerviewwithgroupdate.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.encryption.R
import kotlinx.android.synthetic.main.item_person.view.*


class MyAdapter(val context: Context, val arrayPersons: ArrayList<com.example.encryption.Person>) :
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        lateinit var view: View
        view = LayoutInflater.from(context).inflate(R.layout.item_person, parent, false)
        return ViewHolderPersonDetails(view)

    }

    override fun getItemCount(): Int {
        return arrayPersons.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tviNombre.text = arrayPersons[position].name
        holder.itemView.tviCorreo.text = arrayPersons[position].gmail
        holder.itemView.tviEdad.text = arrayPersons[position].age.toString()

        holder.itemView.cviCard.setOnClickListener {
            val name = (context as Activity).findViewById<View>(R.id.tviName) as TextView
            val gmail = (context).findViewById<View>(R.id.tviGmail) as TextView
            val age = (context).findViewById<View>(R.id.tviAge) as TextView
            val id = (context).findViewById<View>(R.id.tviIndex) as TextView
            name.text = arrayPersons[position].name
            gmail.text = arrayPersons[position].gmail
            age.text = arrayPersons[position].age.toString()
            id.text = "" + position
/*
                val edit = (context ).findViewById<View>(R.id.butEdit) as Button
                val delete = (context ).findViewById<View>(R.id.butDelete) as Button
                edit.isClickable=true
                delete.isClickable=true*/
        }


    }


    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    inner class ViewHolderPersonDetails(itemView: View) : ViewHolder(itemView) {

    }


}